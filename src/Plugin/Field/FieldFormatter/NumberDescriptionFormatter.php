<?php

namespace Drupal\number_description\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'number' formatter.
 *
 * @FieldFormatter(
 *   id = "number_description",
 *   label = @Translation("Number and Description"),
 *   field_types = {
 *     "number_description"
 *   }
 * )
 */
class NumberDescriptionFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $values = $items->getValue();

    foreach ($items as $delta => $element) {
      $elements[$delta] = [
        'number' => [
          '#plain_text' => $values[$delta]['number'],
        ],
        'description' => [
          '#type' => 'processed_text',
          '#text' => $values[$delta]['description'],
          '#format' => 'plain_text',
          '#langcode' => $langcode,
        ],
      ];
    }

    return $elements;
  }

}
