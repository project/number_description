<?php

namespace Drupal\number_description\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'number_description' field type.
 *
 * @FieldType(
 *   id = "number_description",
 *   label = @Translation("Number in text format with description"),
 *   description = @Translation("Stores a number in text format with an associated description."),
 *   default_widget = "number_description_default",
 *   default_formatter = "number_description",
 * )
 */
class NumberDescriptionItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['number'] = DataDefinition::create('string')
      ->setLabel(t('Number in text format'));

    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('Description text'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema['columns']['number'] = [
      'type' => 'varchar',
      'length' => 10,
      'not null' => FALSE,
    ];

    $schema['columns']['description'] = [
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = parent::generateSampleValue($field_definition);

    $random = new Random();
    $values['number'] = rand(0, 20) . 'M';
    $values['description'] = $random->sentences(4);

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $number_value = $this->get('number')->getValue();
    $description_value = $this->get('description')->getValue();

    return empty($number_value) && empty($description_value);
  }

}
