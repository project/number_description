<?php

namespace Drupal\number_description\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'number_description_default' widget.
 *
 * @FieldWidget(
 *   id = "number_description_default",
 *   label = @Translation("Number and Description"),
 *   field_types = {
 *     "number_description"
 *   }
 * )
 */
class NumberDescriptionWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $widget['number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number'),
      '#default_value' => isset($items[$delta]->number) ? $items[$delta]->number : '',
      '#maxlength' => '10',
      '#description' => $this->t('The number in text format.'),
    ];

    $widget['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => isset($items[$delta]->description) ? $items[$delta]->description : '',
      '#maxlength' => '255',
      '#description' => $this->t('The description associated with this number.'),
    ];

    return $widget;
  }

}
